<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Mon blog</title>
	<link href="style.css" rel="stylesheet" /> 
    </head>
        
    <body>
        <h1>Mon super blog !</h1>
        <p>Derniers billets du blog :</p>
 
<?php
// Connexion à la base de données
try
{
	$bdd = new PDO('mysql:host=localhost;dbname=blog;charset=utf8', 'root', '');
}
catch(Exception $e)
{
        die('Erreur : '.$e->getMessage());
}

// On récupère les 5 derniers billets
$reponse = $bdd -> query("SELECT * FROM billets LIMIT 0,5");

while($data = $reponse->fetch()){
    echo '<strong>',htmlspecialchars($data['titre']),'</strong>','<br>';
    echo htmlspecialchars($data['contenu']),'<br>';
    echo 'crée le : '.$data['date_creation'].'<br>';
    ?>
    <a href= <?php echo 'commentaire.php?postid='.$data['id'] ?> >commentaires</a> <br><br>
    <?php
}

// Fin de la boucle des billets
$reponse->closecursor();
?>
</body>
</html>