<?php
try
{
	$bdd = new PDO('mysql:host=localhost;dbname=blog;charset=utf8', 'root', '');
}
catch(Exception $e)
{
        die('Erreur : '.$e->getMessage());
}

if(!isset($_GET['postid'])){header('Location: index.php');}
if(!is_numeric($_GET['postid'])){header('Location: index.php');}
if($_GET['postid']<0){header('Location: index.php');}
$_GET['postid']=(int)$_GET['postid'];

$reponse = $bdd -> query('SELECT * FROM commentaires WHERE id_billet='.$_GET['postid']);
$post = $bdd -> query('SELECT * FROM billets WHERE id='.$_GET['postid']);

$data = $post -> fetch();
if(empty($data)){header('Location: index.php');}
echo 'Post originial : <br>'.'<strong>'.$data['titre'].'</strong>'.'<br>'.$data['contenu'].'<br><br><br>';

while($data = $reponse -> fetch()){
	echo '<strong>'.htmlspecialchars($data['auteur']).'</strong>'.' à écrit le '.htmlspecialchars($data['date_commentaire']).' : <br>';
	echo htmlspecialchars($data['commentaire']).'<br><br>';
} 
$reponse->closecursor();
?>
<a href=index.php>Retour au menu principal</a>

