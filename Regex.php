<p>
<?php
if (isset($_POST['mail']))
{
    $_POST['mail'] = htmlspecialchars($_POST['mail']); // On rend inoffensives les balises HTML que le visiteur a pu rentrer

    if (preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $_POST['mail']))
    {
        echo 'L\'adresse ' . $_POST['mail'] . ' est <strong>valide</strong> !';
    }
    else
    {
        echo 'L\'adresse ' . $_POST['mail'] . ' n\'est pas valide, recommencez !';
    }
}

if (isset($_POST['lien']))
{
    $texte = htmlspecialchars($_POST['lien']); // On rend inoffensives les balises HTML que le visiteur a pu rentrer
    $texte2 = preg_replace("#(https?://[a-z0-9._/-]+\.[a-z]{2,4}\?(.+&?)*) #i", "<a href=$1>$1</a> ", $texte);
    echo "<br>",$texte2;
}
?>
</p>

<form method="post">
<p>
    <label for="mail">Votre mail ?</label> <input id="mail" name="mail" /><br /> 
    <label for="lien">Votre lien ?</label> <input id="lien" name="lien" /><br />
    <input type="submit" value="Vérifier le mail" />
</p>
</form>