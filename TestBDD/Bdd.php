<?php
// Sous WAMP (Windows)
try
{
$bdd = new PDO('mysql:host=localhost;dbname=jeux_video;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
}

// Si tout va bien, on peut continuer
$table='jeux_video';
$possesseur='Patrick';
if(isset($_GET['possesseur']) )  {
	echo 'Variables trouvées dans l\'URL : ' ;
	$possesseur=$_GET['possesseur'];
	echo $possesseur;
}
// On récupère tout le contenu de la table jeux_video
$reponse = $bdd->query('SELECT * FROM jeux_video');
$TailoredRep = $bdd->query('SELECT nom FROM jeux_video WHERE possesseur=\'Patrick\' ');
$SortedRep = $bdd->query('SELECT * FROM jeux_video ORDER BY nom LIMIT 0,5');
$VarRep1 = $bdd->prepare('SELECT * FROM jeux_video WHERE possesseur = ?'); 
$VarRep1 -> execute(array($possesseur));
$VarRep = $bdd->prepare('SELECT * FROM jeux_video WHERE possesseur = :variable'); 
$VarRep -> execute(array('variable' => $possesseur));
$repAlias = $bdd -> query("SELECT UPPER(nom) AS nom_maj FROM jeux_video");
$repAgregat = $bdd -> query("SELECT AVG(prix) AS nom_avg FROM jeux_video WHERE possesseur='Patrick'");
$repNbCons = $bdd -> query("SELECT COUNT(DISTINCT console) AS nbC FROM jeux_video");
$repGroup = $bdd -> query("SELECT AVG(prix) AS pm,possesseur FROM jeux_video WHERE prix<=40 GROUP BY possesseur HAVING pm<=30");


// On affiche chaque entrée une à une
$data= $repAgregat->fetch();
echo 'Prix moyen Patrick : '.$data["nom_avg"];
?> <br> <?php

$data= $repNbCons->fetch();
echo 'Nombre consoles : '.$data["nbC"];
echo '<br>'; 

echo ' Requete variable :';
while ($data = $SortedRep->fetch()){
	?> <br /> <?php 
	echo $data['possesseur'];
}

echo ' Requete groupée :';
while ($data = $repGroup->fetch()){
	?> <br /> <?php 
	echo $data['pm'],' : ',$data['possesseur'];
}


echo ' Requete avec Alias :';
while ($data = $repAlias->fetch()){
	?> <br /> <?php 
	echo $data['nom_maj'];
}

while ($donnees = $reponse->fetch())
{
?>
    <p>
    <strong>Jeu</strong> : <?php echo $donnees['nom']; ?><br />
    Le possesseur de ce jeu est : <?php echo $donnees['possesseur']; ?>, et il le vend à <?php echo $donnees['prix']; ?> euros !<br />
    Ce jeu fonctionne sur <?php echo $donnees['console']; ?> et on peut y jouer à <?php echo $donnees['nbre_joueurs_max']; ?> au maximum<br />
    <?php echo $donnees['possesseur']; ?> a laissé ces commentaires sur <?php echo $donnees['nom']; ?> : <em><?php echo $donnees['commentaires']; ?></em>
   </p>
<?php
}

echo 'Jeux alpha :';
while ($data = $SortedRep->fetch()){
	?> <br /> <?php 
	echo $data['nom'];
}



?> <br /> <?php 
echo 'Patrick possède : ' ;

while ($data = $TailoredRep->fetch()){
	?> <br /> <?php 
	echo $data['nom'];

}

$reponse->closeCursor(); // Termine le traitement de la requête

?>