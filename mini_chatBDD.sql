-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 21 juin 2019 à 11:13
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `minichat`
--

-- --------------------------------------------------------

--
-- Structure de la table `mini_chat`
--

DROP TABLE IF EXISTS `mini_chat`;
CREATE TABLE IF NOT EXISTS `mini_chat` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `datemess` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `mini_chat`
--

INSERT INTO `mini_chat` (`ID`, `pseudo`, `message`, `datemess`) VALUES
(1, 'Test', 'Test', '2019-06-10 00:00:00'),
(2, '1', '2', '2019-06-10 00:00:00'),
(3, '1', '2', '2019-06-10 00:00:00'),
(4, '1', '2', '2019-06-10 00:00:00'),
(5, 'Alex', 'Hello final?', '2019-06-10 00:00:00'),
(6, 'SpamyBoiiiiii', 'Ay', '2019-06-10 00:00:00'),
(7, 'SpamyBoiiiiii', 'Ay', '2019-06-10 00:00:00'),
(8, 'Spam', 'Spam', '2019-06-10 00:00:00'),
(9, 't', 'tt', '2019-06-10 00:00:00'),
(10, 'ef', 'hud', '2019-06-10 00:00:00'),
(11, 'gkerzh', 'vedhvf', '2019-06-10 00:00:00'),
(12, '\'g', 'rfvr', '2019-06-10 00:00:00'),
(23, 'gre', 'gre', '2019-06-10 00:00:00'),
(67, 'hel', 'lo', '2019-06-11 00:00:00'),
(45, 'Marc', 'Page', '2019-06-10 00:00:00'),
(63, 's', 's', '2019-06-11 00:00:00'),
(68, 'z', 'e', '2019-06-11 00:00:00'),
(71, 'f', 'f', '2019-06-11 00:00:00'),
(72, '0', '0', '2019-06-11 00:00:00'),
(73, '0', '0', '2019-06-11 00:00:00'),
(87, '4', '5', '2019-06-11 00:00:00'),
(88, '6', '7', '2019-06-11 00:00:00'),
(89, '7', '8', '2019-06-11 00:00:00'),
(92, '55', '4', '2019-06-11 03:59:59'),
(93, '5', '5', '2019-06-11 04:00:26'),
(102, 'Alex', 'Les cookies sont disponibles pour le pseudo !', '2019-06-11 04:19:45'),
(110, 'Alex', 'Ceci est surement le dernier message ... Le chat fonctionne avec toute la partie \'pour aller plus loin\' implementée !', '2019-06-11 04:52:22'),
(111, 'Alex', 'Hello ! Le systeme de gestion datetime à été modifié !', '2019-06-12 15:28:15'),
(112, 'Alex', 'Maintenant le champ \'date du message\' est bien un DATETIME et plus une STRING', '2019-06-12 15:34:05'),
(113, 'Alex', 'Test', '2019-06-13 20:18:34');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
