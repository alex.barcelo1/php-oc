<?php

$image = imagecreatefromjpeg("photopro.jpg"); 
$hauteur = imagesy($image);
$largeur = imagesx($image);
$miniature = imagecreatetruecolor($largeur/10, $hauteur/10);

imagecopyresampled( $miniature,$image, 0, 0, 0, 0, $largeur/10, $hauteur/10, $largeur, $hauteur);

imagejpeg($miniature, "minimoi.jpg");
?>